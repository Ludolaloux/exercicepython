// For a given number of steps, print out a “staircase” using hashes and spaces.
// run tests with : "npm test Staircase"

function steps(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Steps", () => {
    it("Should print steps", () => {
        expect(steps(3)).toBe("#  \n## \n###\n")
    })
})
