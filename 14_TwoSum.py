// Given an array of numbers, return all pairs that add up to a given sum. The numbers can be used more than once.
// run tests with : "npm test TwoSum"

function twoSum(array, sum) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Two Sum", () => {
    it("Should implement two sum", () => {
        expect(twoSum([1, 2, 2, 3, 4], 4)).toBe([[2, 2], [3, 1]])
    })
})
