// For a given number of levels, print out a “pyramid” using hashes and spaces.
// run tests with : "npm test Pyramid"

function pyramid(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Pyramid", () => {
    it("Should print pyramid", () => {
        expect(pyramid(3)).toBe("  #  \n ### \n#####\n")
    })
})
