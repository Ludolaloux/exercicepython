// Given an array and a size, split the array items into a list of arrays of the given size.
// run tests with : "npm test arraychunking"

function chunk(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Array Chunking", () => {
    it("Should implement array chunking", () => {
        expect(chunk([1, 2, 3, 4], 2)).toBe([[1, 2], [3, 4]])
        expect(chunk([1, 2, 3, 4], 3)).toBe([[1, 2, 3], [4]])
        expect(chunk([1, 2, 3, 4], 5)).toBe([[1, 2, 3, 4]])
    })
})