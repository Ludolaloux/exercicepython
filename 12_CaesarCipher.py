// Given a phrase, substitute each character by shifting it up or down the alphabet by a given integer. 
// If necessary, the shifting should wrap around back to the beginning or end of the alphabet.
// run tests with : "npm test CaesarCipher"

function caesarCipher(phrase, integer) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Caesar Cipher", () => {
    it("Should shift to the right", () => {
        expect(caesarCipher("I love JavaScript!", 100)).toBe("E hkra FwrwOynelp!")
    })
    it("Should shift to the left", () => {
        expect(caesarCipher("I love JavaScript!", -100)).toBe("M pszi NezeWgvmtx!")
    })
})
