# Given a number as an input, print out every integer from 1 to that number. 
# However, when the integer is divisible by 2, print out “Fizz”.
# When it’s divisible by 3, print out “Buzz”.
# When it’s divisible by both 2 and 3, print out “Fizz Buzz”.
# expected behavior :
# python 04_FizzBuzz.py 30
# output: 
'''
1
Fizz
Buzz
Fizz
5
Fizz Buzz
7
Fizz
Buzz
Fizz
11
Fizz Buzz
13
Fizz
Buzz
Fizz
17
Fizz Buzz
19
Fizz
Buzz
Fizz
23
Fizz Buzz
25
Fizz
Buzz
Fizz
29
Fizz Buzz
'''

import sys

def fizzBuzz(input):
    # insert code here

fizzBuzz(int(sys.argv[1]))
