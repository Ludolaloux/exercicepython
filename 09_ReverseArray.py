// Given an array of items, reverse the order.
// run tests with : "npm test reversearray"

function reverseArray(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Reverse Arrays", () => {
    it("Should reverse arrays", () => {
        expect(reverseArray([1, 2, 3, 4])).toBe([4, 3, 2, 1])
        expect(reverseArray([1, 2, 3, 4, 5])).toBe([5, 4, 3, 2, 1])
    })
})
